//
//  Puzzle.h
//  Enigma
//
//  Created by Eleve on 21/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Puzzle : NSObject

@property(nonatomic, strong) NSString *id;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *text;
@property(nonatomic, strong) NSMutableArray *answers;
@property(nonatomic, strong) NSString *user;
@property(nonatomic, strong) NSDate *created;
@property(nonatomic, strong) NSDate *updated;

@end
