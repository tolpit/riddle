//
//  AppDelegate.h
//  Enigma
//
//  Created by Eleve on 21/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

