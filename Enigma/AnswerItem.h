//
//  AnswerItem.h
//  Enigma
//
//  Created by Eleve on 22/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerItem : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@end
