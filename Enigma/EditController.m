//
//  EditController.m
//  Enigma
//
//  Created by Eleve on 21/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import "EditController.h"
#import "Puzzle.h"
#import "AnswerCell.h"

@interface EditController ()  <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *puzzleInput;
@property (weak, nonatomic) IBOutlet UISwitch *checkShare;
@property (weak, nonatomic) IBOutlet UIButton *sendPuzzle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSDictionary *user;

@end

@implementation EditController

- (void)viewDidLoad {
    self.user    = [[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]mutableCopy];
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self setupStyles];
    [self setupPuzzle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupStyles {
    self.puzzleInput.layer.borderWidth = 1.0f;
    self.puzzleInput.layer.borderColor = [[UIColor grayColor] CGColor];
}

- (void)setupPuzzle {
    
    if( self.puzzle.id ) {
        self.puzzleInput.text = self.puzzle.text;
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.puzzle.answers count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AnswerCell *cell = (AnswerCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *answer = self.puzzle.answers[indexPath.row];
    
    if (cell == nil) {
        cell = [[AnswerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.inputAnswer.text = answer[@"text"];
    
    return cell;
}

#pragma mark - send Data

- (IBAction)sendPuzzle:(id)sender {
    
    NSString *text = self.puzzleInput.text;
    NSString *isShare;
    
    if([self.checkShare isOn]) {
        isShare = @"1";
    } else {
        isShare = @"0";
    }
    
    
    NSString *post = [NSString stringWithFormat:@"text=%@&user=%@", text, self.user[@"_id"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",[postData length]];
    NSURL *url;
    
    if( self.puzzle.id ) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://4b5584057a.testurl.ws/api/puzzle/%@", self.puzzle.id]];
    }
    else {
        url = [NSURL URLWithString:@"http://4b5584057a.testurl.ws/api/puzzle"];
    }
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] initWithURL:url];
    
    if( self.puzzle.id ) {
        [request setHTTPMethod:@"PUT"];
    }
    else {
        [request setHTTPMethod:@"POST"];
    }
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *task =
    [[NSURLSession sharedSession] dataTaskWithRequest:request
                                    completionHandler:^(NSData *data,
                                                        NSURLResponse *response,
                                                        NSError *error) {
                                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                        
                                        if( !self.puzzle.id ) {
                                            self.puzzle.id = json[@"data"][@"_id"];
                                            self.puzzle.text = json[@"data"][@"text"];
                                            self.puzzle.answers = json[@"data"][@"answers"];
                                            self.puzzle.user = json[@"data"][@"user"];
                                            self.puzzle.updated = json[@"data"][@"updated_at"];
                                            self.puzzle.created = json[@"data"][@"created_at"];
                                        }
                                        
                                        //Update the answers
                                        [self sendAnswers:self.puzzle.id];
                                    }];
    [task resume];

}

- (void)sendAnswers:puzzleID {
    NSMutableArray *data  = [[NSMutableArray alloc] init];
    NSMutableArray *cells = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < [self.tableView numberOfRowsInSection:0]; ++i) {
        [cells addObject:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]]];
    }
    
    //Get the cell value
    for (AnswerCell *cell in cells) {
        [data addObject:cell.inputAnswer.text];
    }
    
    //Update the answers
    for(NSInteger i = 0; i < [data count]; ++i) {
        NSMutableDictionary *answer = [[NSMutableDictionary alloc] init];
  
        [answer setObject:[data objectAtIndex:i] forKey:@"text"];
        
        if( self.puzzle.answers[i] != NULL ) {
            [answer setObject:self.puzzle.answers[i][@"_id"] forKey:@"_id"];
        }
        
        NSString *post = [NSString stringWithFormat:@"text=%@&puzzle=%@&user=%@", answer[@"text"], puzzleID, self.user[@"_id"]];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",[postData length]];
        NSURL *url;
        
        if( self.puzzle.answers[i] != NULL ) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"http://4b5584057a.testurl.ws/api/answer/%@", answer[@"_id"]]];
        }
        else {
            url = [NSURL URLWithString:@"http://4b5584057a.testurl.ws/api/answer"];
        }
        
        NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] initWithURL:url];
        
        if( self.puzzle.answers[i] != NULL ) {
            [request setHTTPMethod:@"PUT"];
        }
        else {
            [request setHTTPMethod:@"POST"];
        }
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        
        NSURLSessionDataTask *task =
        [[NSURLSession sharedSession] dataTaskWithRequest:request
                                        completionHandler:^(NSData *data,
                                                            NSURLResponse *response,
                                                            NSError *error) {
                                            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                            
                                            NSLog(@"%@", json);
                                            
                                        }];
        [task resume];
    }
}

@end
