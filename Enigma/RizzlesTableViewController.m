//
//  RizzlesTableViewController.m
//  Enigma
//
//  Created by Eleve on 21/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import "RizzlesTableViewController.h"
#import "Puzzle.h"
#import "EditController.h"
#import "AnswerItem.h"

@interface RizzlesTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property(nonatomic, strong) NSMutableArray *puzzles;
@property(nonatomic, strong) NSDictionary *user;

@end

@implementation RizzlesTableViewController

- (void)viewDidLoad {
    self.user    = [[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]mutableCopy];
    self.puzzles = [NSMutableArray array];
    
    [super viewDidLoad];
    
    self.title = @"Tes énigmes";
    
    [self getPuzzles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)getPuzzles {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://4b5584057a.testurl.ws/api/puzzle?user=%@", self.user[@"_id"]]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error) {
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            
            if(!jsonError) {
                
                for(NSDictionary *puzzleDetails in json) {
                    Puzzle *puzzle = [[Puzzle alloc] init];
                    puzzle.id = puzzleDetails[@"_id"];
                    puzzle.name = puzzleDetails[@"name"];
                    puzzle.text = puzzleDetails[@"text"];
                    puzzle.answers = puzzleDetails[@"answers"];
                    puzzle.user = puzzleDetails[@"user"];
                    puzzle.updated = puzzleDetails[@"updated_at"];
                    puzzle.created = puzzleDetails[@"created_at"];
                    
                    [self.puzzles addObject:puzzle];
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }];
    
    [task resume];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.puzzles count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AnswerItem *cell = (AnswerItem*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Puzzle *puzzle        = self.puzzles[indexPath.row];
    
    if (cell == nil) {
        cell = [[AnswerItem alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = puzzle.text;
    
    return cell;
}

- (void)sendToUser:pseudo Game:puzzle {
    NSLog(@"user: %@, puzzle: %@", pseudo, puzzle);
}

- (IBAction)sendTo:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath  = [self.tableView indexPathForRowAtPoint:buttonPosition];
    Puzzle *puzzle          = self.puzzles[indexPath.row];

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Pseudo :" message:@"Quel est votre pseudo ?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSString *pseudo = alert.textFields.firstObject.text;
                                                              
                                                              [self sendToUser:pseudo Game:puzzle];
                                                          }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"A qui ?";
    }];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath  = [self.tableView indexPathForRowAtPoint:buttonPosition];
    Puzzle *puzzle          = self.puzzles[indexPath.row];
    
    if ([[segue identifier] isEqualToString:@"Edit"]) {
        NSLog(@"%@", puzzle);
        EditController *edit = [segue destinationViewController];
        [edit setPuzzle:puzzle];
    }

}

@end
