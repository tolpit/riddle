//
//  AnswerCell.h
//  Enigma
//
//  Created by Eleve on 24/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *inputAnswer;

@end
