//
//  AnswerController.m
//  Enigma
//
//  Created by Eleve on 21/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import "AnswerController.h"
#import "EditController.h"

@interface AnswerController ()



@property (weak, nonatomic) IBOutlet UITextView *puzzleText;
@property (weak, nonatomic) IBOutlet UITextField *answerInput;
@property (weak, nonatomic) IBOutlet UIView *winModal;
@property (weak, nonatomic) IBOutlet UIView *nextModal;
@property (weak, nonatomic) IBOutlet UIView *failModal;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *sendBackButton;

@property(nonatomic, strong) NSDictionary *user;

@end

@implementation AnswerController

@synthesize puzzle;

- (void)viewDidLoad {
    self.user    = [[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]mutableCopy];
    
    [super viewDidLoad];
    [self viewPuzzle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewPuzzle {
    self.puzzleText.text = self.puzzle.text;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)isValidAnswer:answer {
    for(NSDictionary *answerEntry in self.puzzle.answers) {
        if([answerEntry[@"text"] containsString:answer]) {
            return YES;
        }
    }
    
    return NO;
}

- (IBAction)sendAnswer:(id)sender {
    if( [self isValidAnswer:self.answerInput.text]) {
        NSLog(@"correct");

        [self.winModal setHidden:NO];
    }
    else {
        NSLog(@"fail");
        
        [self.failModal setHidden:NO];

    }
}

- (IBAction)sendBack:(id)sender {
    [self.winModal setHidden:YES];
    [self.nextModal setHidden:NO];
}

@end
