//
//  AnswerItem.m
//  Enigma
//
//  Created by Eleve on 22/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import "AnswerItem.h"

@implementation AnswerItem : UITableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.editButton.layer.zPosition = 10;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
