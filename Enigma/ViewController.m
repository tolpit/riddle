//
//  ViewController.m
//  Enigma
//
//  Created by Eleve on 21/03/2016.
//  Copyright © 2016 Gobelins. All rights reserved.
//

#import "ViewController.h"
#import "Puzzle.h"
#import "AnswerController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *writeButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) UIAlertController *alertPseudo;
@property(nonatomic, strong) NSMutableArray *puzzles;
@property(nonatomic, strong) NSDictionary *user;

@end

@implementation ViewController

- (void)viewDidLoad {
    self.user    = [[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]mutableCopy];
    self.puzzles = [NSMutableArray array];

    [super viewDidLoad];
    
    //Get user infos
    if( [self.user objectForKey:@"_id"] == nil ) {
        [self askForPseudo];
    }
    else {
        //NSLog(@"%@", self.user);
    }
    
    //Get list
    [self getPuzzles];
}

- (void)askForPseudo {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Pseudo :" message:@"Quel est votre pseudo ?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSLog(@"Pseudo send");
                                                              
                                                              NSString *pseudo = alert.textFields.firstObject.text;
                                                          
                                                              [self sendPseudo:pseudo];
                                                          }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Pseudo";
    }];
    
    [alert addAction:defaultAction];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (void)sendPseudo:pseudo {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://4b5584057a.testurl.ws/api/user/%@", pseudo]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error) {
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            
            if(!jsonError) {
                
                self.user = json;
                
                [[NSUserDefaults standardUserDefaults] setObject:self.user forKey:@"user"];
                
                NSLog(@"%@", self.user);
            }
        }
    }];
    
    [task resume];

}

- (void)getPuzzles {
    NSURL *url = [NSURL URLWithString:@"http://4b5584057a.testurl.ws/api/puzzle"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error) {
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            
            if(!jsonError) {
                
                for(NSDictionary *puzzleDetails in json) {
                    Puzzle *puzzle = [[Puzzle alloc] init];
                    puzzle.id = puzzleDetails[@"_id"];
                    puzzle.name = puzzleDetails[@"name"];
                    puzzle.text = puzzleDetails[@"text"];
                    puzzle.answers = puzzleDetails[@"answers"];
                    puzzle.user = puzzleDetails[@"user"];
                    puzzle.updated = puzzleDetails[@"updated_at"];
                    puzzle.created = puzzleDetails[@"created_at"];
                    
                    [self.puzzles addObject:puzzle];
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }];
    
    [task resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.puzzles count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Puzzle *puzzle        = self.puzzles[indexPath.row];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = puzzle.text;
    
    return cell;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Answer"]) {
        NSIndexPath *indexPath  = [self.tableView indexPathForSelectedRow];
        Puzzle *puzzle          = self.puzzles[indexPath.row];
        
        AnswerController *answer = [segue destinationViewController];
        [answer setPuzzle:puzzle];
    }
    
    if ([[segue identifier] isEqualToString:@"AnswerRandom"]) {
        Puzzle *puzzle = [self.puzzles objectAtIndex: arc4random() % [self.puzzles count]];
        AnswerController *answer = [segue destinationViewController];
        [answer setPuzzle:puzzle];
    }
}

@end
